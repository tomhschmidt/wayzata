//
//  InstagramLocation.h
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstagramModel.h"
#import <CoreLocation/CoreLocation.h>

@interface InstagramLocation : InstagramModel

@property NSString* name;
@property CLLocationCoordinate2D coordinates;

@end
