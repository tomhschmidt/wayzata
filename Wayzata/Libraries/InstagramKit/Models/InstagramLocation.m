//
//  InstagramLocation.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import "InstagramLocation.h"

@implementation InstagramLocation

- (id)initWithInfo:(NSDictionary *)info {
    self = [super init];
    if(self) {
//        if (IKNotNull(info[kID])) {
//            self.Id = [NSString stringWithFormat:@"%d", [info[kID] intValue]];
//        }
        if(info[kLatitude] && info[kLongitude]) {
            _coordinates = CLLocationCoordinate2DMake([info[kLatitude] doubleValue], [info[kLongitude] doubleValue]);
        }
           
       if(IKNotNull(info[kLocationName])) {
           _name = info[kLocationName];
       }
    }
    
    return self;
}

@end
