//
//  WZFeedViewController.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import "WZFeedViewController.h"
#import "WZEngine.h"
#import "InstagramKit.h"

@interface WZFeedViewController()

@property CLLocationManager* locationManager;
@property CLLocationCoordinate2D curLocation;
@property BOOL locationLoaded;

@property NSArray* photos;
@property UITableView* tableView;
@end

@implementation WZFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLocManager];
    [self requestLocPermissions];
    self.tableView = self.view;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)setupLocManager {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
}

- (void)requestLocPermissions {
    if([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    } else {
        [_locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if(status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [_locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation* latestLoc = [locations lastObject];
    _curLocation = latestLoc.coordinate;
    
    if(!_locationLoaded) {
        _locationLoaded = YES;
        
        self.photos = [[WZEngine sharedEngine] getPhotosNearLocation:_curLocation];
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.photos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    InstagramMedia *photo = [self.photos objectAtIndex:indexPath.row];
    if(photo.location.name) {
        cell.textLabel.text = photo.location.name;
    } else {
        cell.textLabel.text = [NSString stringWithFormat:@"%2.4f, %2.4f", photo.location.coordinates.latitude, photo.location.coordinates.longitude];
    }
    
    cell.detailTextLabel.text = photo.user.username;
    
    return cell;
}

#pragma Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[UIApplication sharedApplication] openURL:((InstagramMedia*)[self.photos objectAtIndex:indexPath.row]).standardResolutionImageURL];
    
}

@end
