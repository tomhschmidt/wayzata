//
//  WZLoginViewController.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/22/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import "WZLoginViewController.h"
#import "InstagramKit.h"
#import "WZEngine.h"

@interface WZLoginViewController ()

@end

@implementation WZLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
    [[InstagramEngine sharedEngine] loginWithScope:IKLoginScopeBasic completionBlock:^(NSError *error) {
        if(error) {
            NSLog(error);
        } else {
            NSLog(@"logged in");
            [[InstagramEngine sharedEngine] getSelfUserDetailsWithSuccess:^(InstagramUser *userDetail) {
                [[WZEngine sharedEngine] setLoggedInUser:userDetail];
                [self performSegueWithIdentifier:@"segueToLoadingView" sender:self];
            } failure:^(NSError *error) {
                NSLog(error);
            }];
        }
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
