//
//  NSMutableArray+QueueExtensions.h
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueExtensions)

- (id) dequeue;
- (void) enqueue:(id)obj;
+ (NSMutableArray*)arrayWithFixedSize:(int)size;
@end
