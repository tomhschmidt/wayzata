//
//  WZEngine.h
//  Wayzata
//
//  Created by Thomas Schmidt on 11/23/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "BMKDTree.h"

@class InstagramUser;

@interface WZEngine : NSObject

@property (nonatomic, retain) InstagramUser* loggedInUser;
@property (nonatomic, retain) NSMutableOrderedSet* usersFollowedByLoggedInUser;
@property (nonatomic, retain) NSMutableDictionary* photosForUser;
@property (nonatomic, retain) BMKDTree* kdTree;

+ (WZEngine*)sharedEngine;
- (void)getAllFollowedUsers:(void (^)(NSSet* followedUsers, NSError* error))completionBlock;
- (void)getAllPhotosFromFollowedUsersWithProgressBlock:(void (^)(double))progressBlock withCompletionBlock:(void (^)(NSError *))completionBlock;
- (NSArray*)getPhotosNearLocation:(CLLocationCoordinate2D)location;

@end
