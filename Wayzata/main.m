//
//  main.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/22/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
