//
//  NSMutableArray+QueueExtensions.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSMutableArray+QueueExtensions.h"

@interface  NSMutableArray (QueueAdditions)
@property int fixedSize;
@end

@implementation NSMutableArray (QueueAdditions)
- (id) dequeue {
    id headObject = [self objectAtIndex:0];
    if (headObject != nil) {
        [self removeObjectAtIndex:0];
    }
    return headObject;
}

- (void) enqueue:(id)anObject {
    if(self.count == 6) {
        [self dequeue];
    }
    [self addObject:anObject];
}

+ (NSMutableArray*)arrayWithFixedSize:(int)size {
    NSMutableArray* array = [NSMutableArray array];
    return array;
}
@end