//
//  WZLoginViewController.h
//  Wayzata
//
//  Created by Thomas Schmidt on 11/22/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WZLoginViewController : UIViewController

@property IBOutlet UIButton* loginButton;

@end
