//
//  WZEngine.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/23/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import "WZEngine.h"
#import "InstagramKit.h"

@interface WZEngine()

@end

@implementation WZEngine

BMKDTreeComparator posComparator;
BMKDTreeScorer posScorer;

+ (WZEngine*)sharedEngine {
    static WZEngine *_sharedEngine = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedEngine = [[WZEngine alloc] init];
        _sharedEngine.usersFollowedByLoggedInUser = [NSMutableOrderedSet orderedSet];
        _sharedEngine.photosForUser = [NSMutableDictionary dictionary];
        posComparator = ^NSComparisonResult(NSUInteger depth, InstagramMedia* datum1, InstagramMedia* datum2) {
            const NSUInteger k = depth % 2;
            double difference = 0;
            if(k == 0) { // latitude
                difference = datum1.location.coordinates.latitude - datum2.location.coordinates.latitude;
            } else if(k == 1) {
                difference = datum1.location.coordinates.longitude - datum2.location.coordinates.longitude;
            }
            return difference ? (difference > 0 ? NSOrderedAscending : NSOrderedDescending) : NSOrderedSame;
        };
        
        posScorer = ^double(NSUInteger depth, BMKDTreeChildType type, InstagramMedia* datum, InstagramMedia* target, BOOL *stop) {
            
            if (BMKDTreeNoChild == type) {
                switch (depth %= 2) {
                    case 0: // lat
                        return abs(datum.location.coordinates.latitude - target.location.coordinates.latitude);
                        break;
                    case 1: // long
                        return abs(datum.location.coordinates.longitude - target.location.coordinates.longitude);
                        break;
                }
            }
            
            return
            pow(datum.location.coordinates.latitude - target.location.coordinates.latitude, 2)* pow(datum.location.coordinates.longitude - target.location.coordinates.longitude,2);
        };
    });
    return _sharedEngine;
}

- (void)getAllFollowedUsers:(void (^)(NSSet*, NSError*))completionBlock {
    [[InstagramEngine sharedEngine] getUsersFollowedByUser:_loggedInUser.Id withSuccess:^(NSArray *objects, InstagramPaginationInfo *paginationInfo) {
        
        [[[WZEngine sharedEngine] usersFollowedByLoggedInUser] addObjectsFromArray:objects];
        
        if(paginationInfo && paginationInfo.nextURL) {
            [self getAllFollowedUsersFromPaginationInfo:paginationInfo withFinalBlock:completionBlock];
        } else {
            completionBlock(_usersFollowedByLoggedInUser, nil);
        }

    } failure:^(NSError *error) {
        completionBlock(nil, error);
    }];
}

- (void)getAllFollowedUsersFromPaginationInfo:(InstagramPaginationInfo*)paginationInfo withFinalBlock:(void (^)(NSSet*, NSError*))completionBlock {
    [[InstagramEngine sharedEngine] getPaginatedItemsForInfo:paginationInfo withSuccess:^(NSArray *objects, InstagramPaginationInfo *pi) {
        [[[WZEngine sharedEngine] usersFollowedByLoggedInUser] addObjectsFromArray:objects];
        if(pi.nextURL) {
            [self getAllFollowedUsersFromPaginationInfo:pi withFinalBlock:completionBlock];
        } else {
            completionBlock(_usersFollowedByLoggedInUser, nil);
        }
    } failure:^(NSError *error) {
        NSLog([error description]);
    }];
}

- (void)getAllPhotosFromFollowedUsersWithProgressBlock:(void (^)(double))progressBlock withCompletionBlock:(void (^)(NSError *))completionBlock {
    __block double numFinished = 0;
    NSMutableSet* allPhotosWithLocation = [NSMutableSet set];
    for(int i = 0; i < _usersFollowedByLoggedInUser.count; i++) {
        InstagramUser* user = [_usersFollowedByLoggedInUser objectAtIndex:i];
        if([user.username isEqualToString:@"whalebot"] || [user.username isEqualToString:@"juliushui"]) {
            ++numFinished;
            continue;
        }
        [self getAllPhotosFromUser:user withBlock:^(NSMutableArray * photos, NSError * error) {
            ++numFinished;
            progressBlock(numFinished / _usersFollowedByLoggedInUser.count);
            
            if(numFinished == _usersFollowedByLoggedInUser.count) {
                NSPredicate* locationPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                    return ((InstagramMedia*)evaluatedObject).location;
                }];
                for(int j = 0; j < _usersFollowedByLoggedInUser.count; j++) {
                    NSArray* photosWithLocation = [((InstagramUser*)[_usersFollowedByLoggedInUser objectAtIndex:j]).recentMedia filteredArrayUsingPredicate:locationPredicate];
                    [allPhotosWithLocation addObjectsFromArray:photosWithLocation];
                }
                self.kdTree = [BMKDTree treeWithSet:allPhotosWithLocation comparator:posComparator];
                completionBlock(nil);
            }
        }];
    }
}

- (void)getAllPhotosFromUser:(InstagramUser*)user withBlock:(void (^)(NSMutableArray*, NSError*))completionBlock {
    NSLog(@"Getting photos for %@", user.username);
    [[InstagramEngine sharedEngine] getMediaForUser:user.Id count:1000 maxId:nil withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        NSLog(@"Retrieved %d photos for %@", media.count, user.username);
        [user.recentMedia addObjectsFromArray:media];
        if(paginationInfo) {
            NSLog(@"Getting more photos with pagniation info for %@", user.username);
            [self getAllPhotosFromUser:user withPaginationInfo:paginationInfo withFinalBlock:completionBlock];
        } else {
            completionBlock(user.recentMedia, nil);
        }
    } failure:^(NSError *error) {
        NSLog([error description]);
    }];
}

- (void)getAllPhotosFromUser:(InstagramUser*)user withPaginationInfo:(InstagramPaginationInfo*)paginationInfo withFinalBlock:(void (^)(NSMutableArray*, NSError*))completionBlock{
    [[InstagramEngine sharedEngine] getPaginatedItemsForInfo:paginationInfo withSuccess:^(NSArray *media, InstagramPaginationInfo *pi) {
        NSLog(@"Retrieved %d photos for %@", media.count, user.username);
        [user.recentMedia addObjectsFromArray:media];
        if(pi) {
            NSLog(@"Getting more photos with pagniation info for %@", user.username);
            [self getAllPhotosFromUser:user withPaginationInfo:pi withFinalBlock:completionBlock];
        } else {
            completionBlock(user.recentMedia, nil);
        }
    } failure:^(NSError *error) {
        
    }];
}

- (NSArray*)getPhotosNearLocation:(CLLocationCoordinate2D)location {
    InstagramMedia* dummyMedia = [[InstagramMedia alloc] init];
    dummyMedia.location = [[InstagramLocation alloc] init];
    dummyMedia.location.coordinates = location;
    return [_kdTree kNearestObjectsToObject:dummyMedia usingScorer:posScorer];
}

@end
