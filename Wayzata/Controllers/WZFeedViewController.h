//
//  WZFeedViewController.h
//  Wayzata
//
//  Created by Thomas Schmidt on 11/30/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface WZFeedViewController : UITableViewController<CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@end
