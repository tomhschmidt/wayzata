//
//  WZLoadingViewController.m
//  Wayzata
//
//  Created by Thomas Schmidt on 11/23/14.
//  Copyright (c) 2014 Thomas Schmidt. All rights reserved.
//

#import "WZLoadingViewController.h"
#import "InstagramKit.h"
#import "WZEngine.h"

@interface WZLoadingViewController ()
@property (strong, nonatomic) IBOutlet UILabel *loadingProgressLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *loadingProgressView;

@end

@implementation WZLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadFollowedUsers];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadFollowedUsers {
    [[WZEngine sharedEngine] getAllFollowedUsers:^(NSSet *followedUsers, NSError *error) {
        if(error) {
            
        } else {
            [self loadPhotosFromFollowedUsers];
        }
    }];
}

- (void)loadPhotosFromFollowedUsers {
    [[WZEngine sharedEngine] getAllPhotosFromFollowedUsersWithProgressBlock:^(double progress) {
        self.loadingProgressLabel.text = [NSString stringWithFormat:@"%.2f", progress];
        self.loadingProgressView.progress = progress;
    } withCompletionBlock:^(NSError * error) {
        [self performSegueWithIdentifier:@"segueToFeedView" sender:self];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
